import Vue from 'vue'
import Router from 'vue-router'
import CreateItemList from '@/components/CreateItemList'
import GetList from '@/components/GetList'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'CreateItemList',
      component: CreateItemList
    },
    {
      path: '/getList',
      name: 'GetList',
      component: GetList
    },
    {path: '/404',
      component: PageNotFound}, {path: '*', redirect: '/404'}
  ]
})
