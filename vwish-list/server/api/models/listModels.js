'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemSchema = Schema({
  title : String,
  category : String
});

var ListSchema = new Schema({
	name: String,
	items: [ItemSchema]
});

var List = mongoose.model('List', ListSchema);

module.exports = List;