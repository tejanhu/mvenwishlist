'use strict';

module.exports = function(app){
	var lists = require('../controllers/listController');

	app.route('/lists')
	.get(lists.list_all_lists)
	.post(lists.create_a_list);

	app.route('/lists/:_id')
	.get(lists.read_a_list)
	.put(lists.update_a_list)
	.delete(lists.delete_a_list);


};