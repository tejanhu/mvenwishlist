'use strict';

var mongoose = require('mongoose'),
List = mongoose.model('List');

exports.insert_tolist = function(req, res){

    List.insert({}, function(err, list){
        if(err)
            res.send(err);
        res.json(list);
    });
};

exports.create_a_list = function(req, res){
var new_list = new List(req.body);
new_list.save(function(err, list){

        if(err)
            res.send(err);
        res.json(list);
    });
};

exports.read_a_list = function(req, res){

    List.findById(req.params.listId, function(err, list){
        if(err)
            res.send(err);
        res.json(list);
    });
};

exports.list_all_lists = function(req, res){

    List.find({}, function(err, list){
        if(err)
            res.send(err);
        res.json(list);
    });
};

exports.update_a_list = function(req, res, next){
   List.findByIdAndUpdate({_id: req.params._id}, req.body).then(function(){
    List.findOne({_id: req.params._id}).then(function(list){
      res.send(list);
    })  
   });   
};

exports.delete_a_list = function(req, res){
    List.findByIdAndRemove({_id: req.params._id}).then(function(list){
        res.send(list);
    });
};
